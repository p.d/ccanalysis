#pragma once

#include <string>
#include <vector>
#include <iostream>
#include <filesystem>
#include <boost/program_options.hpp>
#include <chrono>
#include <cassert>

std::vector<long long> channel_merge(const std::vector<long long> &data, const uint16_t newchan, const std::vector<uint16_t> &mergelist, const bool keep = true);
void write_tags(const std::string &fn, const std::vector<long long> &data);
