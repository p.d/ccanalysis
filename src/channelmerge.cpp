#include "channelmerge.h"

import io;
import tools;

namespace bpo = boost::program_options;

int main(int argc, char **argv)
{ 
    std::vector<uint16_t> mergelist = {};
    uint16_t newchan = 0;
    std::string suffix = "";
    bool keep = true;
    /*
     * get files from prompt
     */
    std::vector<std::string> tagfiles;
    try {
        bpo::options_description args("Arguments");
        args.add_options()
            ("help,h", "prints this message")
            ("channel-list,o", bpo::value<std::vector<uint16_t>>()->multitoken(), "list of channels which shall be merged into one")
            ("new-channel,n",  bpo::value<uint16_t>(), "number of newly created channel")
            ("output-suffix,s",  bpo::value<std::string>(), "suffix appended to filename when writing new tagfile")
            ("keep,k",  bpo::value<bool>()->default_value(true), "keep old channels in file [default: true]")
            ("input-files,i", bpo::value<std::vector<std::string>>(), "Tag files to split")
            ;
        bpo::options_description cmdline_options;
        cmdline_options.add(args);
        bpo::positional_options_description args_pos;
        args_pos.add("input-files", -1);
        
        bpo::variables_map args_vm;
        store(bpo::command_line_parser(argc, argv).
              options(cmdline_options).positional(args_pos).run(), args_vm);
        notify(args_vm);
        
        if (args_vm.count("help") != 0) {
            std::cout << "Usage: ./channelmerge -o <chn> <chn> [<chn>...] -n <chn> -s <str> input-files\n";
            std::cout << "merges all tags appearing in a list of channels into a newly created channel.\n";
            std::cout << args << "\n";
            return 0;
        }

        if (args_vm.count("input-files") != 0) {
            tagfiles = args_vm["input-files"].as< std::vector<std::string> >();
        } else {
            std::cout << "Error: No input files specified." << std::endl;
            return 2;
        }
        
        if ((args_vm.count("channel-list") == 0)) {
            std::cout << "Error: No list of channels to merge specified." << std::endl;
            return 2;
        } else {
            mergelist = args_vm["channel-list"].as<std::vector<uint16_t>>();
        }
        
        if ((args_vm.count("new-channel") == 0)) {
            std::cout << "Error: No merge channel specified." << std::endl;
            return 2;
        } else {
            newchan = args_vm["new-channel"].as<uint16_t>();
        }
        
        if ((args_vm.count("output-suffix") == 0)) {
            std::cout << "Error: No output file name suffix specified. Please specify, this tool will not overwrite files." << std::endl;
            return 2;
        } else {
            suffix = args_vm["output-suffix"].as<std::string>();
        }
        
        keep = args_vm["keep"].as<bool>();
        
        
    }
    catch(std::exception& e) {
        std::cerr << "error: " << e.what() << "\n";
        return 1;
    }
    catch(...) {
        std::cerr << "Exception of unknown type!\n";
        return 1;
    }
    
    std::cout << "merging channels [";
    for (auto c: mergelist) {
        std::cout << c << ", ";
    }
    std::cout << "\b\b] into channel " << newchan << std::endl;
    
    std::vector<long long> data;
    std::vector<long long> data_merged;
    std::string new_fn = "";
    for (const auto &fn: tagfiles) {
        data.clear();
        data_merged.clear();
        
        new_fn = std::filesystem::path(fn).stem().string() + suffix + std::filesystem::path(fn).extension().string();
        
        std::cout << "input file: " << fn << std::endl;
        if (cctools::readtags(fn, data)) {
            continue;
        }
        
        data_merged = channel_merge(data, newchan, mergelist, keep);
        
        write_tags(new_fn, data_merged);
    }
}

std::vector<long long> channel_merge(const std::vector<long long> &data, const uint16_t newchan, const std::vector<uint16_t> &mergelist, const bool keep){
    assert(mergelist.size() > 0 && "No channel list to merge received.");
    
    std::vector<long long> data_merged;

    if (keep) {
        for (size_t idx = 0; idx < data.size(); idx+=2) {
            if (std::find(mergelist.begin(), mergelist.end(), data[idx]) != mergelist.end()) {
                data_merged.emplace_back(newchan);
                data_merged.emplace_back(data[idx+1]);
            }
            data_merged.emplace_back(data[idx]);
            data_merged.emplace_back(data[idx+1]);
        }
    } else {
        data_merged = data;
        for (size_t idx = 0; idx < data_merged.size(); idx+=2) {
            if (std::find(mergelist.begin(), mergelist.end(), data_merged[idx]) != mergelist.end()) {
                data_merged[idx] = newchan;
            }
        }
    }
    
    return data_merged;
}

void write_tags(const std::string &fn, const std::vector<long long> &data) {
    std::string oldextension = std::filesystem::path(fn).extension();
    
    std::cout << "writing...\n";
    if (oldextension == ".h5") {
        cctools::writeHDFtags(fn, data, cctools::HDF5_COMP_ALG_ZLIB, 6);
    } else if ((oldextension == ".txt") || (oldextension == ".tsv")) {
        cctools::lltoTSV(fn, data);
    } else if ((oldextension == ".tags") || (oldextension == ".zst")) {
        cctools::writecapnptags(fn, data, true, 6);
    }
}
