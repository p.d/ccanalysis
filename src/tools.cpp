module;

#include <string>
#include <sstream>
#include <iostream>
#include <vector>
#include <regex>
#include <iomanip>
#include <numeric>

export module tools;

export namespace cctools {
    /********************************************************************************
    *** find and replace in a string (find 1, NOT N)
    */
    bool stringreplace(std::string& str, const std::string& from, const std::string& with) {
        size_t start_pos = str.find(from);
        if(start_pos == std::string::npos) {
            return false;
        }
        str.replace(start_pos, from.length(), with);
        return true;
    }

    /********************************************************************************
    *** parse string containing patternspatterns
    */
    std::vector<std::vector<uint16_t>> parse_patterns(const std::string &instring) {
        std::string patstr = instring;
        if (!(patstr.starts_with("{{"))) {
            std::cout << "pattern string malformatted: " << patstr << std::endl;
        }
        if (!(patstr.ends_with("}}"))) {
            std::cout << "pattern string malformatted: " << patstr << std::endl;
        }
        
        patstr.erase(0,1);
        patstr.erase(patstr.length()-1,1);
        
        size_t vec_start_idx = 0;
        size_t vec_stop_idx = 0;
        std::vector<size_t> sep_idxs = {};
        size_t curr_sep_idx = 0;
        
        std::string vector_string;
        std::vector<uint16_t> pattern = {};
        std::vector<std::vector<uint16_t>> patterns = {};
        std::string channelstr;
        // separate patterns
        while (vec_stop_idx < patstr.length()-1) {
            vec_start_idx =  patstr.find('{', vec_start_idx);
            vec_stop_idx = patstr.find('}', vec_start_idx);
            vector_string = patstr.substr(vec_start_idx, vec_stop_idx-vec_start_idx+1);
            
            sep_idxs = {};
            sep_idxs.push_back(vector_string.find('{'));
            curr_sep_idx = vector_string.find(',', 0);
            while(curr_sep_idx != std::string::npos) {
                sep_idxs.push_back(curr_sep_idx);
                curr_sep_idx = vector_string.find(',',curr_sep_idx+1);
            }
            sep_idxs.push_back(vector_string.find('}'));
            
            //get channels
            pattern = {};
            for (size_t i=0; i< sep_idxs.size()-1; ++i) {
                channelstr = vector_string.substr(sep_idxs[i]+1, sep_idxs[i+1]-sep_idxs[i]-1);
                channelstr = std::regex_replace(channelstr, std::regex(R"([\D])"), "");
                pattern.push_back(stoi(channelstr));
            }
            patterns.push_back(pattern);
            
            vec_start_idx = vec_stop_idx;
        }
        
        return patterns;
    }

    std::string toLower(const std::string &ins) {
        std::string os = ins;
        std::transform(ins.begin(), ins.end(), os.begin(), [](unsigned char c){ return std::tolower(c); });
        
        return os;
    }

    /********************************************************************************
    *** get index list that sorts a vector
    */
    template <typename T>
    std::vector<size_t> get_sort_perm(const std::vector<T> &v) {
    std::vector<size_t> idx(v.size());
    std::iota(idx.begin(), idx.end(), 0);
    std::sort(idx.begin(), idx.end(), [&v](size_t i, size_t j) {return v[i] < v[j];});
    return idx;
    }

    /********************************************************************************
    *** reorder vector according to index list
    */
    template <typename T>
    std::vector<T> vector_permute(const std::vector<T> &v, const std::vector<size_t> &idx) {
        std::vector<T> sorted_vec(v.size());
        std::transform(idx.begin(), idx.end(), sorted_vec.begin(), [&](size_t i){ return v[i]; });
        return sorted_vec;
    }
        
    /********************************************************************************
    *** create string of integer, with fixed width
    */
    template<typename T, typename U>
    requires std::integral<T> && std::unsigned_integral<U>
    std::string fixed_width_intstr(const T number, const U width){
        std::ostringstream ss;
        if (number < 0) {
            ss << '-';
        }
        ss << std::setfill('0') << std::setw(width) << (number < 0 ? -number : number);
        return ss.str();
    }

    /********************************************************************************
    *** print elements of vector
    */
    template<typename T>
    void print_vector(const std::vector<T> v) {
        std::cout << "[";
        for (T e : v) {
            if (e != v.back()) {
                std::cout << e << ", ";
            } else {
                std::cout << e << "]" << std::endl;
            }
        }
    }

    /********************************************************************************
    *** turn vector of vectors into vector
    */
    template<typename T>
    std::vector<T> flatten(const std::vector<std::vector<T>> vv) {
        std::vector<T> result;
        for (auto v: vv) {
            result.insert(result.end(), v.begin(), v.end());
        }
        return result;
    }

    /********************************************************************************
    *** find unique entries in vector
    */
    template<typename T>
    std::vector<T> unique(const std::vector<T> v) {
        std::vector<T> result;
        for (auto e: v) {
            if (std::find(result.begin(), result.end(), e) == result.end()) {
                result.push_back(e);
            }
        }
        return result;
    }

    /********************************************************************************
    *** create vector holding range of values
    */
    template<class T, typename std::enable_if<std::is_floating_point<T>::value>::type...>
    std::vector<T> arange(const T start, const T stop, const T step) {
        std::vector<T> values;
        for (T value = start; value < stop; value += step) {
            values.emplace_back(value);
        }
        return values;
    }

    template<class T, typename std::enable_if<std::is_integral<T>::value>::type...>
    std::vector<T> arange(const T start, const T stop, const T step) {
        std::vector<T> values;
        for (T value = start; value < stop; value += step) {
            values.emplace_back(value);
        }
        return values;
    }

    template<class T>
    requires std::integral<T>
    T roundto(const T num, const T to)
    {
        if (to == 0) {
            return num;
        }
        
        T r = abs(num) % to;
        if (r == 0) {
            return num;
        }
        
        if (num < 0) {
            if (2*r > to) {
                return num + r - to;
            } else {
                return num + r;
            }
        } else {
            if (2*r > to) {
                return num - r + to;
            } else {
                return num - r;
            }
        }
    }
} //namespace cctools
