#pragma once

#include <algorithm>
#include <iostream>
#include <string>
#include <vector>
#include <chrono>
#include <filesystem>
#include <boost/program_options.hpp>

#define MIN_COMPRESSION_LEVEL 0
#define DEFAULT_COMPRESSION_LEVEL 6
#define MAX_COMPRESSION_LEVEL 9
